const IBMEventStreams = require('node-rdkafka');
const {IBMEventStreamsConfig} = require("../credentials")
class Consumer {
    constructor(consumerProperties){
        this.consumer = {};
        this.consumerProperties = consumerProperties;
        const topicOpts = {
            'auto.offset.reset': 'latest'
        };
        this.consumer = new IBMEventStreams.KafkaConsumer(new IBMEventStreamsConfig().consumerConfig, topicOpts);
    }
    connect(){
        this.consumer.connect();
    }
    setUp(){
        this.consumer.on('event.log', log=>this.consumerProperties.onEventLog(log));
        this.consumer.on('event.error', error=>this.consumerProperties.onEventError(error));
        this.consumer.on('ready',()=> {
            console.log(`The consumer has connected, subscribing to ${this.consumerProperties.topic}.`);
            this.consumer.subscribe([this.consumerProperties.topic]);
            this.consumer.consume();
        })
        this.consumer.on('data',data=>{
            this.consumerProperties.onData(data)
            this.consumer.disconnect();
        })
    }
}
class ConsumerRunner{
  run(){
    const consumerProperties = {
        onEventLog(log){
            console.log(`In Event Log ${log}`);
        },
        onEventError(error){
            console.error(`Error from consumer:${JSON.stringify(error,null,2)}`);
        },
        "topic":"kafka-nodejs-console-sample-topic-company-data",
        onData(message){
            //const innerMessage = JSON.parse(message.value.toString())
            console.log(`Received Message ${JSON.stringify(JSON.parse(message.value.toString()),null,2)}`);
        }
    }
    const consumer = new Consumer(consumerProperties);
    consumer.setUp()
    consumer.connect();
  }
}
new ConsumerRunner().run();
module.exports={Consumer}
