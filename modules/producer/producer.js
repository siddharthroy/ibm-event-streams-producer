const IBMEventStreams = require('node-rdkafka');
const {IBMEventStreamsConfig} = require("../credentials")

class Producer{
    constructor(producerProperties){
        this.producer = {};
        this.producerProperties = producerProperties;
        const topicOpts = {
            'request.required.acks': -1,
            'produce.offset.report': true
        };
        this.producer = new IBMEventStreams.Producer(new IBMEventStreamsConfig().producerConfig, topicOpts);
    }
    connect(){
        this.producer.connect();
        console.log(`Connected..calling ready handler..`)
    }
    disconnect(){
        this.producer.disconnect(()=>console.log(`Producer Disconnected`))
    }
    setUp(){
        this.producer.setPollInterval(this.producerProperties.pollInterval);
        this.producer.on('event.log', log=> this.producerProperties.onEventLog(log));
        this.producer.on('event.error',error=> this.producerProperties.onEventError(error));
        this.producer.on('delivery-report', (error, deliveryReport)=>this.producerProperties.onDeliveryReport(error,deliveryReport));
        this.producer.on('ready', ()=>{
                try {
                    console.log(`Sending message to Event Streams...`)
                    this.producer.produce(this.producerProperties.topic, this.producerProperties.partition, this.producerProperties.message, this.producerProperties.key);
                    this.disconnect();
                } catch (err) {
                    console.error(`Failed sending message ${this.producerProperties.message}`);
                    console.error(err);
                }
            })
    }
}
class ProducerRunner{
    run(){
        const producerProperties = {
            "pollInterval":100,
            "onEventLog" : function(log){ console.log(log) },
            "onEventError": function(error){console.error(error)},
            "onDeliveryReport": function(error, deliveryReport){ 
                if(error)
                    console.error(error)
                else
                    console.log(`Message delievered on ${new Date(deliveryReport.timestamp)}`)

            },
            "topic":"kafka-nodejs-console-sample-topic",
            "partition": null,
            "message" : new Buffer(JSON.stringify({event:"getCompanies",eventGenerator:"TYS"})),
            "key": null
        }
        const producer = new Producer(producerProperties);
        producer.setUp()
        producer.connect();
    }
}
new ProducerRunner().run();
module.exports = {Producer}