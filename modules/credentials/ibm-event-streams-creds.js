const config = require("./ibm-event-streams.json")
class IBMEventStreamsConfig{
    get driverConfig(){
        const driverConfig = {
            "metadata.broker.list": config.ibm_event_streams_security_key.kafka_brokers_sasl,
            "security.protocol": 'sasl_ssl',
            "ssl.ca.location": config.ibm_event_streams_security_key.calocation,
            "sasl.mechanisms": 'PLAIN',
            "sasl.username": 'token',
            "sasl.password": config.ibm_event_streams_security_key.api_key,
            "broker.version.fallback": "0.10.0",
            "log.connection.close" : false
        };
        return driverConfig;
    }
    get producerConfig(){
        const producerConfig = {
            "client.id": config.clientIds.producerClientId,//TODO: May not be needed.
            "dr_msg_cb": true ,
            ...this.driverConfig
        };
        return producerConfig;
    }
    get consumerConfig(){
        const consumerConfig = {
        'client.id': config.clientIds.consumerClientId, //TODO: May not be needed.
        'group.id': 'kafka-nodejs-console-sample-group',//TODO: May not be needed.
        ...this.driverConfig
        }
        return consumerConfig;
       }
}
module.exports = {IBMEventStreamsConfig}